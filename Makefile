
LDLIBS = $(shell pkg-config --libs alsa) -lm

CFLAGS = -g $(shell pkg-config --cflags alsa)

SRC = \
	input-event.c

EXE = 	\
	input-event

all: $(EXE)


clean:
	rm $(EXE)

zip:
	zip all.zip Makefile $(SRC)
